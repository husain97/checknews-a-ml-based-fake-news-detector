from flask import Flask, render_template, request, json, escape
from newspaper import Article
import pickle
import nltk
import tldextract

app = Flask(__name__)

@app.route("/")
def checkNews():
    return render_template('card.html')

@app.route('/classify', methods=['POST'])
def classify():

	if request.method == 'POST':
		url = request.form['url']
		
		extract = tldextract.extract(url)
		domain = extract.domain
		article = Article(url, 'en')
		article.download()
		article.parse()
		article.nlp()
		summary = article.summary
		news = summary.replace('"',' ')
		
		#Loading the Model
		filename = "model.save"
		model_load = pickle.load(open(filename, 'rb'))
		
		#Loading the Vectorizer
		filename = "vectorizer.save"
		vectorizer_load = pickle.load(open(filename, 'rb'))
		
		test_data = [news]
		_test=vectorizer_load.transform(test_data)
		pred = model_load.predict(_test)  
		prediction = pred[0]
		return json.dumps({'status':'OK','Domain':domain,'News':news,'Prediction':prediction})
	else:
		render_template('card.html')

if __name__ == "__main__":
    app.run(debug = 'True')