import pickle
import html

filename = "model.save"
model_load = pickle.load(open(filename, 'rb'))
print(type(model_load))

filename = "vectorizer.save"
vectorizer_load = pickle.load(open(filename, 'rb'))
print(type(vectorizer_load))

data='Farmers in Nashik hailed new agriculture reforms. They are hopeful that agriculture reforms will be beneficial for them. One farmer said, “The new farmer bill is good and is beneficial. Earlier, prices were used to be decided by merchants but now it is up to us. We can sell our goods anywhere in the country. We are thankful to Modi government for the new bill.” The other farmer thanked Modi government for bringing the bill. “Those who are opposing new farm bills are completely wrong.'

data = html.escape(data)

test = [data]
_test=vectorizer_load.transform(test)
pred = model_load.predict(_test)
pred_ = pred[0]
print(pred_)
