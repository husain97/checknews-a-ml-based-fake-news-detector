$(function(){
	$('#news_submit').click(function(){
		var news = $('#inputNews').val();
			
		if(news.match(/[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi)){
			
			$.ajax({
			url: '/classify',
			data: "url="+news,
			type: 'POST',
			beforeSend: function(){
				$('#loader').removeClass('hidden')
			},
			complete: function(){
				$('#loader').addClass('hidden')
			},
			success: function(response){
				console.log(response);
				var response_j = JSON.parse(response);
				var news_summary = response_j['News'];
				var news_domain = response_j['Domain'];
				var news_prediction = response_j['Prediction'];
				console.log(news_domain);
				
				document.getElementById("news_summary").innerHTML = news_summary + "<br>" + "<h3 style='font-size:15px'>News Domain: " + news_domain + "</p>";
				
				//news_prediction = 'Fake News';
				
				if(news_prediction == 'Real News'){
					
					document.getElementById("news_prediction").innerHTML =  "<div class='card text-white bg-success mb-5' style='max-width: 45rem;margin-top:43px;padding: 10px 10px 10px 10px'>" +
				"<div class='card-body'>" +
						"<h5 class='card-title' style='font-size: 4em;'>" + news_prediction + "</h5>" +
						"</div>" +
						"</div>"
					
				}
				else{
					
					document.getElementById("news_prediction").innerHTML =  "<div class='card text-white bg-danger mb-5' style='max-width: 45rem;margin-top:43px;padding: 10px 10px 10px 15px'>" +
				"<div class='card-body'>" +
						"<h5 class='card-title' style='font-size: 4em;'>" + news_prediction + "</h5>" +
						"</div>" +
						"</div>"
				}
			},
			error: function(error){
				console.log(error);
			}
		  });
		}
		else{
			window.alert("Please Enter a Valid URL");
			document.getElementById("inputNews").focus();
		}
	});
});