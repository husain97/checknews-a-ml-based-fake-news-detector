#1 - Fake News, 0 - Real News
#Importing Dependencies
print("Importing Dependencies...\n")
import numpy as np
import pandas as pd
import itertools
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.metrics import accuracy_score, confusion_matrix
import matplotlib.pyplot as plt
import pickle

#Silencing the false positive warnings
pd.options.mode.chained_assignment = None  # default='warn'

#Importing Dataset
print("Importing Original Dataset...\n")
data = pd.read_csv('train.csv', index_col=False)
df = data

#Filling Missing Values in Data
print("Preprocessing Dataset...\n")
df['title'] = df['title'].fillna(value = 'Missing Value')
df = df.dropna()
df.head()
df['length'] = df.loc[:,'text'].str.len()
df[df['length'] < 50].count()
df = df.drop(df['text'][df['length'] < 50].index, axis=0)

#FEATURES AND LABELS
print("Defining Features and Labels...\n")
df['label'].replace([1,0],['Fake News','Real News'], inplace=True)
Y = df.label

#Preparing Training Dataset And Test Dataset
print("Preparing Training Dataset And Test Dataset...\n")
X_train,X_test,Y_train,Y_test=train_test_split(df['text'], Y, test_size=0.2, random_state=7)

#Initializing a TfidfVectorizer
print("Initializing a TfidfVectorizer...")
tfidf_vectorizer=TfidfVectorizer(stop_words='english',norm=None,min_df=0)
TFIDF_train=tfidf_vectorizer.fit_transform(X_train) 
TFIDF_test=tfidf_vectorizer.transform(X_test)

#Saving the vectorizer
print("Saving the vectorizer...")
filename = "vectorizer.save"
pickle.dump(tfidf_vectorizer, open(filename, 'wb'))
print("TFIDF Vectorizer Saved: vectorizer.save\n")

#Initializing a PassiveAggressiveClassifier
print("Initializing a PassiveAggressiveClassifier...")
model = PassiveAggressiveClassifier(max_iter=50)
model.fit(TFIDF_train,Y_train)
Y_pred = model.predict(TFIDF_test)
acc_score = accuracy_score(Y_test,Y_pred)
print(f'Accuracy: {round(acc_score*100,2)}%')

#Saving the model
print("Saving the model...")
filename = "model.save"
pickle.dump(model, open(filename, 'wb'))
print("Model File Saved: model.save\n")

'''
#CONFUSION MATRIX
print("Plotting confusion Matrix\n")
labels_=['Fake News','Real News']
cm=confusion_matrix(Y_test,Y_pred, labels=['Fake News','Real News'])
print(cm)
fig = plt.figure()
ax = fig.add_subplot(111)
cax = ax.matshow(cm)
plt.title('Confusion matrix of the classifier')
fig.colorbar(cax)
ax.set_xticklabels([''] + labels_)
ax.set_yticklabels([''] + labels_)
plt.xlabel('Predicted')
plt.ylabel('True')
plt.show()
'''